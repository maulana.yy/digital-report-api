/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
  /***************************************************************************
   *                                                                          *
   * Make the view located at `views/homepage.ejs` your home page.            *
   *                                                                          *
   * (Alternatively, remove this and add an `index.html` file in your         *
   * `assets` directory)                                                      *
   *                                                                          *
   ***************************************************************************/

  "/": { view: "pages/homepage" },

  "get /user": "UserController.getAll",
  "get /user/:id/detail": "UserController.getOne",
  "post /login": "UserController.login",
  "post /user": "UserController.create",
  "post /user/reset-password": "UserController.resetPassword",
  "put /user/:id": "UserController.update",
  "delete /user/:id": "UserController.delete",

  "get /lab": "LabController.getAll",
  "get /lab/:id": "LabController.getOne",
  "get /lab/code": "LabController.getCode",
  "post /lab": "LabController.create",
  "put /lab/:id": "LabController.update",
  "delete /lab/:id": "LabController.delete",

  "get /control-point": "ControlPointController.getAll",
  "get /control-point/parameter": "ControlPointController.getAllWithParameter",
  "get /control-point/:id": "ControlPointController.getOne",
  "get /control-point/code": "ControlPointController.getCode",
  "post /control-point": "ControlPointController.create",
  "put /control-point/:id": "ControlPointController.update",
  "delete /control-point/:id": "ControlPointController.delete",

  //Paramater
  "get /parameter": "ParameterController.getParameter",
  "get /parameter/:id": "ParameterController.getDetailParameter",
  "post /parameter": "ParameterController.createParameter",
  "put /parameter/:id": "ParameterController.updateParameter",
  "delete /parameter/:id": "ParameterController.deleteParameter",

  "get /parameter-oracle/code": "ParameterController.getParameterOracleCode",
  "get /parameter-oracle/:id/detail-okp": "ParameterController.getParameterDetailOKP",
  "get /parameter-oracle/okp" : "ParameterController.getParameterOKP",
  
  "get /parameter-value": "ParameterController.getParameterValue",
  "get /parameter-value/:id" : "ParameterController.getDetailParameterValue",
  "post /parameter-value": "ParameterController.createParameterValue",
  "put /parameter-value/:id": "ParameterController.updateParameterValue",
  "delete /parameter-value/:id": "ParameterController.deleteParameterValue",

  "get /form-parameter": "FormController.getFormSetting",
  "get /form-parameter/:id": "FormController.getOneFormSetting",
  "post /form-parameter": "FormController.createFormSetting",
  "put /form-parameter/:id": "FormController.updateFormSetting",
  "delete /form-parameter/:id": "FormController.deleteFormSetting",
  "get /form-parameter/:id/code": "FormController.getFormSettingCode",
  "get /form-parameter/code/list": "FormController.getFormCode",
  "get /form-parameter/:id/parameter": "FormController.getParameterForm",

  "get /master-form": "MasterFormController.getMasterFormSetting",
  "get /master-form/code": "MasterFormController.getMasterFormCode",
  "get /master-form/:id": "MasterFormController.getOneMasterFormSetting",
  "post /master-form": "MasterFormController.createMasterFormSetting",
  "put /master-form/:id": "MasterFormController.updateMasterFormSetting",
  "delete /master-form/:id": "MasterFormController.deleteMasterFormSetting",

  //Form Data
  "get /form-data": "FormController.getFormData",
  "get /form-data/:id": "FormController.getOneFormData",
  "get /form-data/:id/value/:form_master_id": "FormController.getFormValue",
  "post /form-data": "FormController.createFormData",
  "put /form-data/:id": "FormController.updateFormData",
  "put /form-data/:id/approve": "FormController.approveFormData",
  "put /form-data/:id/close": "FormController.closeFormData",
  "get /form-data/print/:id": "FormController.FormPrint",
  "get /form-data-parameter": "FormController.getFormDataParameter",

  //Setting Ewon
  "get /setting/ewon": "SettingController.getEwon",
  "get /setting/ewon/:id": "SettingController.getOneEwon",
  "get /setting/ewon/code": "SettingController.getEwonCode",
  "post /setting/ewon": "SettingController.createEwon",
  "put /setting/ewon/:id": "SettingController.updateEwon",
  "delete /setting/ewon/:id": "SettingController.deleteEwon",

  //Setting Oracle
  "get /setting/oracle": "SettingController.getOracle",
  "get /setting/oracle/:id": "SettingController.getOneOracle",
  "post /setting/oracle": "SettingController.createOracle",
  "put /setting/oracle/:id": "SettingController.updateOracle",
  "delete /setting/oracle/:id": "SettingController.deleteOracle",

  "get /setting/type-okp": "SettingController.getTypeOKP",
  "get /setting/type-okp/:id": "SettingController.getOneTypeOKP",
  "get /setting/type-okp/code": "SettingController.getTypeOKPCode",
  "post /setting/type-okp": "SettingController.createTypeOKP",
  "put /setting/type-okp/:id": "SettingController.updateTypeOKP",
  "delete /setting/type-okp/:id": "SettingController.deleteTypeOKP",
  /***************************************************************************
   *                                                                          *
   * More custom routes here...                                               *
   * (See https://sailsjs.com/config/routes for examples.)                    *
   *                                                                          *
   * If a request to a URL doesn't match any of the routes in this file, it   *
   * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
   * not match any of those, it is matched against static assets.             *
   *                                                                          *
   ***************************************************************************/
};
