/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const jwtData = {
  expiresIn: "2 day",
};

module.exports = {
  getAll: async (req, res) => {
    const { page, limit } = req.query;
    let query = {
      deletedAt: null,
    };
    const sort = req.query.sort ? req.query.sort : "intUserID ASC";
    try {
      let users = [];
      const pagination = {
        page: parseInt(page) - 1 || 0,
        limit: parseInt(limit) || 20,
      };

      const count = await M_Users.count(query);
      if (count > 0) {
        const queries = await sails.sendNativeQuery(
          `
              SELECT * from m_users where deletedAt is null order by createdAt DESC limit $2 offset $1
                  `,
          [pagination.page * pagination.limit, pagination.limit]
        );
        users = queries.rows;
      }

      const numberOfPages = Math.ceil(count / pagination.limit);
      const nextPage = parseInt(page) + 1;
      const meta = {
        page: parseInt(page),
        perPage: pagination.limit,
        previousPage: parseInt(page) > 1 ? parseInt(page) - 1 : false,
        nextPage: numberOfPages >= nextPage ? nextPage : false,
        pageCount: numberOfPages,
        total: count,
      };

      const data = {
        data: users,
        meta: meta,
      };

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getOne: async (req, res) => {
    const { id } = req.params;
    try {
      let data = await M_Users.findOne({
        where: {
          id: id,
          deletedAt: null,
        },
      });

      if (!data) {
        sails.helpers.errorResponse("data not found", "failed").then((resp) => {
          res.status(401).send(resp);
        });
      }

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  create: async (req, res) => {
    const { user } = req;
    let { body } = req;
    try {
      const data = await M_Users.create({
        txtUsername: body.username,
        txtPassword: body.password,
        txtName: body.name,
        txtDepartment: body.department,
        txtRelationship: body.relationship,
        txtRole: body.role,
        intLabID: body.lab,
        txtCreatedBy: user.id,
      }).fetch();

      await M_User_History.create({
        intUserID: user.id,
        txtAction: user.name + "create new user",
      });

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  update: async (req, res) => {
    const { user, params } = req;
    let { body } = req;
    try {
      const member = await M_Users.findOne({
        where: {
          id: params.id,
          deletedAt: null,
        },
      });

      if (!member) {
        sails.helpers.errorResponse("user not found", "failed").then((resp) => {
          res.status(401).send(resp);
        });
      }

      const data = await M_Users.update({
        id: params.id,
      }).set({
        txtName: body.name,
        txtDepartment: body.department,
        txtRelationship: body.relationship,
        txtRole: body.role,
        intLabID: body.lab,
        txtUpdatedBy: user.id,
        updatedAt: new Date(),
      });

      await M_User_History.create({
        intUserID: user.id,
        txtAction: user.name + "update user " + params.id,
      });

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  delete: async (req, res) => {
    const { user, params } = req;
    try {
      const member = await M_Users.findOne({
        where: {
          id: params.id,
          deletedAt: null,
        },
      });

      if (!member) {
        sails.helpers.errorResponse("user not found", "failed").then((resp) => {
          res.status(401).send(resp);
        });
      }

      const data = await M_Users.update({
        id: params.id,
      }).set({
        txtDeletedBy: user.id,
        deletedAt: new Date(),
      });

      await M_User_History.create({
        intUserID: user.id,
        txtAction: user.name + "delete user " + params.id,
      });

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  login: async (req, res) => {
    const { body } = req;
    try {
      let user = await M_Users.findOne({
        where: { txtUsername: body.username },
        select: [
          "id",
          "txtName",
          "txtUsername",
          "txtPassword",
          "txtRole",
          "intLabID",
          "createdAt",
          "updatedAt",
        ],
      });
      const dateNow = new Date();
      const dateCreateUser = user.createdAt;

      var Difference_In_Time = dateNow.getTime() - dateCreateUser.getTime();
      var Difference_In_Days = Math.floor(
        Difference_In_Time / (1000 * 3600 * 24)
      );
      if (!user) {
        sails.helpers.errorResponse("", "user not found").then((resp) => {
          res.status(400).send(resp);
        });
        //   } else if (user.intRoleID != 1 && Difference_In_Days >= 90) {
        //     sails.helpers.errorResponse("", "user Expired").then((resp) => {
        //       res.status(400).send(resp);
        //     });
      } else {
        const userData = {
          id: user.id,
          name: user.txtName,
          username: user.txtUsername,
          txtRole: user.txtRole,
          lab_id: user.intLabID,
        };
        const valid = await bcrypt.compare(body.password, user.txtPassword);
        if (valid) {
          userData.token = jwt.sign(
            userData,
            sails.config.session.secret,
            jwtData
          );

          await M_User_History.create({
            intUserID: user.id,
            txtAction: user.name + "login",
          });

          sails.helpers.successResponse(userData, "success").then((resp) => {
            res.ok(resp);
          });
        } else {
          sails.helpers
            .errorResponse("Username or Password Wrong...", "failed")
            .then((resp) => {
              res.status(400).send(resp);
            });
        }
      }
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  resetPassword: async (req, res) => {
    const { body } = req;
    try {
      let user = await M_Users.findOne({
        where: { txtUsername: body.username },
        select: [
          "id",
          "txtName",
          "txtUsername",
          "txtPassword",
          "createdAt",
          "updatedAt",
        ],
      });

      if (!user) {
        sails.helpers.errorResponse({}, "user not found").then((resp) => {
          res.status(400).send(resp);
        });
      } else {
        const match = await bcrypt.compare(body.password, user.txtPassword);

        if (match) {
          const data = await M_Users.update({
            id: user.id,
          }).set({
            txtPassword: body.new_password,
          });

          await M_User_History.create({
            intUserID: user.id,
            txtAction: user.name + "reset password",
          });
          sails.helpers.successResponse({}, "success").then((resp) => {
            res.ok(resp);
          });
        } else {
          sails.helpers
            .errorResponse("Username or Password Wrong...", "failed")
            .then((resp) => {
              res.status(400).send(resp);
            });
        }
      }
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
};
