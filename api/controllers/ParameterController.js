/**
 * ParameterController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const oracledb = require("oracledb");
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;

const configDB = {
  user: "XXKMIDQM",
  password: "ora3won01",
  connectString: "10.104.50.30:1521/KMIPROD",
};

module.exports = {
  getParameter: async (req, res) => {
    const { page, limit } = req.query;
    let query = {
      deletedAt: null,
    };
    const sort = req.query.sort ? req.query.sort : "createdAt DESC";
    try {
      let parameterData = [];
      const pagination = {
        page: parseInt(page) - 1 || 0,
        limit: parseInt(limit) || 20,
      };
      const count = await M_Parameter.count(query);
      if (count > 0) {
        const queries = await sails.sendNativeQuery(
          `
                  SELECT m_parameter.intParameterID AS id,m_parameter.txtName AS txtName,m_parameter.txtTipe AS txtTipe
                  ,IF (m_parameter.txtTipe = 'mesin',(SELECT txtTopic FROM m_ewon_subscriber WHERE intEwonSubsSettingID = m_parameter.intEwonSubsSettingID),'-') AS txtTopic,
                  m_parameter.createdAt FROM m_parameter WHERE m_parameter.deletedAt IS NULL ORDER BY m_parameter.intParameterID ASC LIMIT $2 OFFSET $1
                    `,
          [pagination.page * pagination.limit, pagination.limit]
        );
        parameterData = queries.rows;
      }

      const numberOfPages = Math.ceil(count / pagination.limit);
      const nextPage = parseInt(page) + 1;
      const meta = {
        page: parseInt(page),
        perPage: pagination.limit,
        previousPage: parseInt(page) > 1 ? parseInt(page) - 1 : false,
        nextPage: numberOfPages >= nextPage ? nextPage : false,
        pageCount: numberOfPages,
        total: count,
      };

      const data = {
        data: parameterData,
        meta: meta,
      };

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getDetailParameter: async (req, res) => {
    const { id } = req.params;
    try {
      let data = await M_Parameter.findOne({
        where: {
          id: id,
        },
      });

      if (!data) {
        sails.helpers
          .errorResponse("parameter is empty", "failed")
          .then((resp) => {
            res.status(400).send(resp);
          });
      }

      if (data.txtTipe == "formula") {
        const formula = await M_Formula.findOne({
          where: {
            txtParameterID: data.txtName,
          },
          select: ["txtParameterOperator", "txtOperator", "intValueOperator"],
        });

        data["formula"] = formula;
      }

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  createParameter: async (req, res) => {
    const { user } = req;
    let { body } = req;
    try {

      const data = await M_Parameter.create({
        txtName: body.oracle_id,
        txtTestCode : body.test_oracle_id,
        txtTipe: body.tipe,
        intEwonSubsSettingID: body.topic_id,
        txtCreatedBy: user.id,
      }).fetch();
      if (body.tipe == "formula") {
        await M_Formula.create({
          txtParameterID : body.oracle_id,
          intValueOperator : body.operator_value,
          txtOperator : body.operator,
          txtParameterOperator : body.operator_parameter,
          txtCreatedBy: user.id,
        })
      }

      await M_User_History.create({
        intUserID : user.id,
        txtAction : user.name + "create new parameter"
      })

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  updateParameter: async (req, res) => {
    const { user, params } = req;
    let { body } = req;
    try {
      const parameter = await M_Parameter.findOne({
        where: {
          id: params.id,
          deletedAt: null,
        },
      });

      if (!parameter) {
        sails.helpers
          .errorResponse("parameter data not found", "failed")
          .then((resp) => {
            res.status(400).send(resp);
          });
      }

      const data = await M_Parameter.update({
        id: params.id,
      }).set({
        txtName: body.oracle_id,
        txtTipe: body.tipe,
        txtTestCode : body.test_oracle_id,
        intEwonSubsSettingID: body.topic_id,
        txtUpdatedBy: user.id,
        updatedAt: new Date(),
      });

      await M_User_History.create({
        intUserID : user.id,
        txtAction : user.name + "update parameter "+params.id
      })

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  deleteParameter: async (req, res) => {
    const { user,params } = req;
    try {
        const parameter = await M_Parameter.findOne({
            where: {
              id: params.id,
              deletedAt: null,
            },
          });
    
          if (!parameter) {
            sails.helpers
              .errorResponse("parameter data not found", "failed")
              .then((resp) => {
                res.status(400).send(resp);
              });
          }
    
          const data = await M_Parameter.update({
            id: params.id,
          }).set({
            deletedAt: new Date(),
          });
          
          await M_User_History.create({
            intUserID : user.id,
            txtAction : user.name + "delete lab "+params.id
          })

          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getParameterOracleCode: async (req, res) => {
    try {
      let data = [];
      conn = await oracledb.getConnection(configDB);

      queryCP =
        "SELECT DISTINCT TEST_CODE FROM XXKMIDQM.KMI_DQM_SPECPARAMTRS_V ORDER BY TEST_CODE ASC";

      const execQueryCP = await conn.execute(queryCP);
      data = execQueryCP.rows;

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getParameterDetailOKP: async (req, res, next) => {
    const { id } = req.params;
    try {
      conn = await oracledb.getConnection(configDB);

      const queryLOTNUMBERS = await conn.execute(
        `SELECT LOT_NUMBER,ITEM_CODE,DESCRIPTION,EXPIRATION_DATE,ACT_STR_DT FROM KMI_DQM_BATCHLOTNUMBERS_V WHERE BATCH_NO = :id order by LOT_NUMBER asc`,
        [id]
      );

      const data = queryLOTNUMBERS.rows;

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getParameterOKP: async (req, res, next) => {
    const { query } = req;
    try {
      const CP = await M_Form.findOne({
        where: {
          id: query.form_id,
        },
        select: ["intControlPointID"],
      });

      let cpText = CP.intControlPointID.toString();

      console.log(cpText)
      
      const paramQuery = await sails.sendNativeQuery(
        `SELECT m_type_okp.txtName FROM m_type_okp,m_type_okp_cp WHERE m_type_okp.intTypeOKP = m_type_okp_cp.intTypeOKPID AND m_type_okp_cp.intControlPointID IN ('${cpText}')`,
        []
      );

      const result = paramQuery.rows

      console.log("RESULT")
      console.log(result)
      typeOKP = result.map((x) => x.txtName);

      conn = await oracledb.getConnection(configDB);

      let rawQuery =
        "SELECT DISTINCT BATCH_NO as batch_number,CREATION_DATE FROM KMI_DQM_BATCHLOTNUMBERS_V";
      if (typeOKP.length > 0) {
        rawQuery += " WHERE";

        for (let x = 0; x < typeOKP.length; x++) {
          const element = typeOKP[x];
          if (x > 0) {
            rawQuery += ` OR`;
          }
          rawQuery += ` KMI_DQM_BATCHLOTNUMBERS_V.BATCH_NO LIKE '${element}%'`;
        }
      }

      rawQuery += " ORDER BY KMI_DQM_BATCHLOTNUMBERS_V.CREATION_DATE DESC";

      const resultQuery = await conn.execute(rawQuery);
      let data = resultQuery.rows;

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getParameterValue: async (req, res) => {
    const { page, limit } = req.query;
    let query = {
      deletedAt: null,
    };        
    const sort = req.query.sort ? req.query.sort : "id ASC";
    try {
      let params = [];
      const pagination = {
        page: parseInt(page) - 1 || 0,
        limit: parseInt(limit) || 20,
      };

      const count = await M_Parameter_Value.count(query);
      if (count > 0) {
        params = await M_Parameter_Value.find(query)
          .skip(pagination.page * pagination.limit)
          .limit(pagination.limit)
          .sort(sort);
      }

      const numberOfPages = Math.ceil(count / pagination.limit);
      const nextPage = parseInt(page) + 1;
      const meta = {
        page: parseInt(page),
        perPage: pagination.limit,
        previousPage: parseInt(page) > 1 ? parseInt(page) - 1 : false,
        nextPage: numberOfPages >= nextPage ? nextPage : false,
        pageCount: numberOfPages,
        total: count,
      };

      const data = {
        data: params,
        meta: meta,
      };

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  getDetailParameterValue: async (req, res) => {
    const { id } = req.params;
    try {
      let data = await M_Parameter_Value.findOne({
        where: {
          id: id,
        },
      });

      if (!data) {
        sails.helpers
          .errorResponse("parameter is empty", "failed")
          .then((resp) => {
            res.status(400).send(resp);
          });
      }

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  createParameterValue: async (req, res) => {
    const { user } = req;
    let { body } = req;
    try {
      const params = body.map(x => {
        return {
          txtParameter : x.TEST_CODE,
          txtCustomValue : x.CUSTOM_VALUE
        }
      })

      const data = await M_Parameter_Value.createEach(params).fetch();
      
      await M_User_History.create({
        intUserID : user.id,
        txtAction : user.name + "create new custom parameter value"
      })

      sails.helpers.successResponse({}, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  updateParameterValue: async (req, res) => {
    const { user, params } = req;
    let { body } = req;
    try {
      const parameter = await M_Parameter_Value.findOne({
        where: {
          id: params.id,
          deletedAt: null,
        },
      });

      if (!parameter) {
        sails.helpers
          .errorResponse("parameter data not found", "failed")
          .then((resp) => {
            res.status(400).send(resp);
          });
      }

      console.log(params)
      console.log('==========')
      console.log(body)

      const data = await M_Parameter_Value.update({
        id: params.id,
      }).set({
        txtParameter: body.parameter.toString(),
        txtCustomValue: body.custom_value,
        txtUpdatedBy: user.id,
        updatedAt: new Date(),
      });

      await M_User_History.create({
        intUserID : user.id,
        txtAction : user.name + "update custom parameter "+params.id
      })

      sails.helpers.successResponse(data, "success").then((resp) => {
        res.ok(resp);
      });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
  deleteParameterValue: async (req, res) => {
    const { user,params } = req;
    try {
        console.log("DELETE PARAMETER VALUE")
        const parameter = await M_Parameter_Value.findOne({
            where: {
              id: params.id,
              deletedAt: null,
            },
          });
    
          if (!parameter) {
            sails.helpers
              .errorResponse("parameter data not found", "failed")
              .then((resp) => {
                res.status(400).send(resp);
              });
            
              return
          }
    
          const data = await M_Parameter_Value.update({
            id: params.id,
          }).set({
            deletedAt: new Date(),
          });
        
          await M_User_History.create({
            intUserID : user.id,
            txtAction : user.name + "delete custom parameter "+params.id
          })

          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
    } catch (err) {
      console.log("ERROR : ", err);
      sails.helpers.errorResponse(err.message, "failed").then((resp) => {
        res.status(400).send(resp);
      });
    }
  },
};
