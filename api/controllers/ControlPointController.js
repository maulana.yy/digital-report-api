/**
 * ControlPointController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
    getAll: async (req, res) => {
        const { page, limit } = req.query;
        let query = {
          deletedAt: null,
        };
        const sort = req.query.sort ? req.query.sort : "createdAt DESC";
        try {
          let controlPoints = [];
          const pagination = {
            page: parseInt(page) - 1 || 0,
            limit: parseInt(limit) || 20,
          };
    
          const count = await M_Control_Point.count(query);
          if (count > 0) {
            const queries = await sails.sendNativeQuery(
              `
              SELECT m_control_point.intControlPointID ,m_control_point.txtName AS txtName, m_lab.txtName AS labTxtName,m_control_point.createdAt AS createdAt FROM m_lab,m_control_point WHERE 
    m_lab.intLabID = m_control_point.intLabID AND m_control_point.deletedAt is null ORDER BY m_control_point.intControlPointID ASC LIMIT $2 OFFSET $1
               `,
              [pagination.page * pagination.limit, pagination.limit]
            );
    
            controlPoints = queries.rows;
          }
    
          const numberOfPages = Math.ceil(count / pagination.limit);
          const nextPage = parseInt(page) + 1;
          const meta = {
            page: parseInt(page),
            perPage: pagination.limit,
            previousPage: parseInt(page) > 1 ? parseInt(page) - 1 : false,
            nextPage: numberOfPages >= nextPage ? nextPage : false,
            pageCount: numberOfPages,
            total: count,
          };
    
          const data = {
            data: controlPoints,
            meta: meta,
          };
    
          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },
      getAllWithParameter: async (req, res) => {
        try {
          const queries = await sails.sendNativeQuery(
            `
            SELECT m_control_point.intControlPointID AS ID,m_control_point.txtName,COUNT(m_parameter.intParameterID)AS total_parameter FROM m_control_point,m_parameter 
            WHERE m_control_point.intControlPointID = m_parameter.intControlPointID GROUP BY m_control_point.intControlPointID `
          );
          const controlPoints = queries.rows;
    
          const data = {
            data: controlPoints,
          };
    
          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },
      getOne: async (req, res) => {
        const { id } = req.params;
        try {
          const queries = await sails.sendNativeQuery(
            `
            SELECT m_control_point.intControlPointID AS id,m_control_point.txtName AS txtName, m_lab.txtName AS labTxtName,m_control_point.createdAt AS createdAt FROM m_lab,m_control_point WHERE m_control_point.deletedAt IS NULL 
            AND m_lab.intLabID = m_control_point.intLabID AND m_control_point.intControlPointID = $1
            `,
            [id]
          );
    
          const areas = queries.rows[0];
    
          const data = {
            data: areas,
          };
    
          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },
      getCode: async (req, res) => {
        try {
          const controlPoints = await M_Control_Point.find({
            where: {
              deletedAt: null,
            },
            select: ["id", "txtName"],
          });
    
          const data = {
            data: controlPoints,
          };
    
          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },
      create: async (req, res) => {
        const { user } = req;
        let { body } = req;
        try {
          const controlPoint = await M_Control_Point.create({
            txtName: body.name,
            intLabID: body.lab_id,
            txtCreatedBy: user.id,
          }).fetch();
    
          await M_User_History.create({
            intUserID: user.id,
            txtAction: user.name + "create new control point",
          });
    
          sails.helpers.successResponse(controlPoint, "success").then((resp) => {
            res.ok(resp);
          });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },
      update: async (req, res) => {
        const { user, params } = req;
        let { body } = req;
        try {
          const controlPoint = await M_Control_Point.findOne({
            where: {
              id: params.id,
              deletedAt: null,
            },
          });
    
          if (!controlPoint) {
            sails.helpers.errorResponse("user not found", "failed").then((resp) => {
              res.status(401).send(resp);
            });
          }
    
          const updateControlPoint = await M_Control_Point.update({
            id: params.id,
          }).set({
            txtName: body.name,
            intLabID: body.lab_id,
            txtUpdatedBy: user.id,
            updatedAt: new Date(),
          });
    
          await M_User_History.create({
            intUserID: user.id,
            txtAction: user.name + "update control point " + params.id,
          });
    
          sails.helpers
            .successResponse(updateControlPoint, "success")
            .then((resp) => {
              res.ok(resp);
            });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },
      delete: async (req, res) => {
        const { user, params } = req;
        try {
          const controlPoint = await M_Control_Point.findOne({
            where: {
              id: params.id,
              deletedAt: null,
            },
          });
    
          if (!controlPoint) {
            sails.helpers.errorResponse("control point not found", "failed").then((resp) => {
              res.status(401).send(resp);
            });

            return
          }
    
          const data = await M_Control_Point.update({
            id: params.id,
          }).set({
            txtDeletedBy: user.id,
            deletedAt: new Date(),
          });
    
          await M_User_History.create({
            intUserID: user.id,
            txtAction: user.name + "delete control point " + params.id,
          });
    
          sails.helpers.successResponse(data, "success").then((resp) => {
            res.ok(resp);
          });
        } catch (err) {
          console.log("ERROR : ", err);
          sails.helpers.errorResponse(err.message, "failed").then((resp) => {
            res.status(400).send(resp);
          });
        }
      },

};

