/**
 * M_Ewon_Subscriber.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    id: {
      type: "number",
      columnName: "intEwonSubsSettingID",
      autoIncrement: true,
    },
    txtTopic: {
      type : "string",
      columnName : "txtTopic"
    },
    txtStatus: {
      type: "boolean",
      columnName : "txtStatus"
    },
  },

};

